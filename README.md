# CICD Challenge


We appreciate you taking the time to participate a cicd challenge. 
In the next step we would like you :

- ADD version control like git and push the code to your private repo
- RUN the application localy ( build, run migrations, test, run the app )
- ADD gitlab cicd file 
- RUN add stage for build and test


### Application contains:

* As a User i can add, delete and modify note
* As a User i can see a list of all note
* As a User i can filter notes via tags
* As a User i must be logged in, in order to view/add/delete/etc and their notes


### Usage:

* To build the application: docker-compose build
* To run the applications: docker-compose up
* To run the tests: docker-compose run --rm app sh -c "python manage.py test && flake8"
* To run the migration : docker-compose run --rm app sh -c "python manage.py migrate"
